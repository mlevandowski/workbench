init:
	mkvirtualenv ../workbench
	pip install -r requirements.txt
	python manage.py syncdb
	python manage.py migrate

test:
	nosetests -v tests/* src/*

doc: 
	make -C docs html
