--extra-index-url=http://dist.pinaxproject.com/dev/
--extra-index-url=http://dist.pinaxproject.com/alpha/

Django==1.4.1
pinax-theme-bootstrap==2.0.4
pinax-theme-bootstrap-account==1.0b2
django-user-accounts==1.0b3
django-forms-bootstrap==2.0.3.post1
django-admin-tools
django-debug-toolbar
django-extensions
feedparser
south
sphinx
metron==1.0
pinax-utils==1.0b1.dev3
#testing / debuggin
nose #test runner
coverage #test coverage
git+git://github.com/jbalogh/django-nose.git#egg=django-nose
pep8
pyflakes
pylint

#load external bitbucket repo apps
hg+https://bitbucket.org/schinckel/django-jsonfield#egg=jsonfield
hg+https://bitbucket.org/salvator/django-admintools-bootstrap#egg=admintools_bootstrap
