from workbench.models import BlockMap
from django.contrib import admin


class BlockMapAdmin(admin.ModelAdmin):
    list_display = ('name', 'user')

admin.site.register(BlockMap)
