# forms.py
from django.forms import ModelForm
from workbench.models import BlockMap

class BlockMapForm(ModelForm):
    class Meta:
        model = BlockMap
        exclude = ('created_by',)