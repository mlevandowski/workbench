from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from workbench.views import BlockMapCreate, BlockMapListView, BlockMapDelete, BlockMapEdit

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', BlockMapListView.as_view(), name="home"),
    url(r'^blockmap/add/', BlockMapCreate.as_view(), name="blockmap_add"),
    url(r'blockmap/(?P<pk>\d+)/delete/$', BlockMapDelete.as_view(), name='blockmap_delete'),
    url(r'blockmap/(?P<pk>\d+)/$', BlockMapEdit.as_view(), name='blockmap_edit'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^docs/$', 'docs'),
    url(r'^account/', include("account.urls")),
)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#urlpatterns += staticfiles_urlpatterns()
