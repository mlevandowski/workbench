/**
Library is under GPL License (GPL)

Copyright (c) 2012 Andreas Herz

**/


/**
 * @class graphiti
 * global namespace declarations
 * 
 * @private
 */
var graphiti = 
{
    geo: {
    },

    io:{
        json:{},
        png:{},
        svg:{}  
    },
    
    util : {
    },

    shape : {
    	basic:{},
        arrow:{},
        node: {},
        note: {},
        diagram:{},
        analog:{},
        icon:{},
        widget:{}
    },
    
    policy : {
    },
    
    command : {
    },

    decoration:{
    	connection:{}
    }, 
    
    layout: {
        connection :{},
	    anchor :{},
	    mesh :{},
	    locator: {}
    },
    
    
    ui :{
    	
    },
    
    isTouchDevice : (
            //Detect iPhone
            (navigator.platform.indexOf("iPhone") != -1) ||
            //Detect iPod
            (navigator.platform.indexOf("iPod") != -1)||
            //Detect iPad
            (navigator.platform.indexOf("iPad") != -1)
        )
    
};

if(typeof graphitiPath === "undefined"){
    graphitiPath = "../../";
}

// loading the lib
//
$LAB
.script(graphitiPath+"lib/shifty.js")
.script(graphitiPath+"lib/raphael.js")
.script(graphitiPath+"lib/jquery-1.8.1.min.js").wait()
.script(graphitiPath+"lib/jquery-ui-1.8.23.custom.min.js").wait()
.script(graphitiPath+"lib/jquery.layout.js")
.script(graphitiPath+"lib/jquery.autoresize.js")
.script(graphitiPath+"lib/jquery-touch_punch.js")
.script(graphitiPath+"lib/jquery.contextmenu.js")
.script(graphitiPath+"lib/rgbcolor.js")
.script(graphitiPath+"lib/canvg.js")
.script(graphitiPath+"lib/Class.js")
.script(graphitiPath+"lib/json2.js").wait()

.script(graphitiPath+"src/util/Color.js")
.script(graphitiPath+"src/util/ArrayList.js")
.script(graphitiPath+"src/util/UUID.js")
.script(graphitiPath+"src/geo/PositionConstants.js")
.script(graphitiPath+"src/geo/Point.js")
.script(graphitiPath+"src/geo/Rectangle.js")

.script(graphitiPath+"src/command/CommandType.js")
.script(graphitiPath+"src/command/Command.js")
.script(graphitiPath+"src/command/CommandStack.js")
.script(graphitiPath+"src/command/CommandStackEvent.js")
.script(graphitiPath+"src/command/CommandStackEventListener.js")
.script(graphitiPath+"src/command/CommandMove.js")
.script(graphitiPath+"src/command/CommandResize.js")
.script(graphitiPath+"src/command/CommandConnect.js")
.script(graphitiPath+"src/command/CommandReconnect.js")
.script(graphitiPath+"src/command/CommandDelete.js")
.script(graphitiPath+"src/command/CommandAdd.js")

.script(graphitiPath+"src/layout/connection/ConnectionRouter.js")
.script(graphitiPath+"src/layout/connection/DirectRouter.js")
.script(graphitiPath+"src/layout/connection/ManhattanConnectionRouter.js")
.script(graphitiPath+"src/layout/connection/ManhattanBridgedConnectionRouter.js")
.script(graphitiPath+"src/layout/connection/BezierConnectionRouter.js")

.script(graphitiPath+"src/layout/mesh/MeshLayouter.js")
.script(graphitiPath+"src/layout/mesh/ExplodeLayouter.js")
.script(graphitiPath+"src/layout/mesh/ProposedMeshChange.js")

.script(graphitiPath+"src/layout/locator/Locator.js")
.script(graphitiPath+"src/layout/locator/PortLocator.js")
.script(graphitiPath+"src/layout/locator/InputPortLocator.js")
.script(graphitiPath+"src/layout/locator/OutputPortLocator.js")
.script(graphitiPath+"src/layout/locator/ConnectionLocator.js")
.script(graphitiPath+"src/layout/locator/ManhattanMidpointLocator.js")
.script(graphitiPath+"src/layout/locator/TopLocator.js")
.script(graphitiPath+"src/layout/locator/BottomLocator.js")
.script(graphitiPath+"src/layout/locator/LeftLocator.js")
.script(graphitiPath+"src/layout/locator/RightLocator.js")
.script(graphitiPath+"src/layout/locator/CenterLocator.js")

.script(graphitiPath+"src/policy/EditPolicy.js")
.script(graphitiPath+"src/policy/DragDropEditPolicy.js")
.script(graphitiPath+"src/policy/RegionEditPolicy.js")
.script(graphitiPath+"src/policy/HorizontalEditPolicy.js")
.script(graphitiPath+"src/policy/VerticalEditPolicy.js")

.script(graphitiPath+"src/Canvas.js")
.script(graphitiPath+"src/Figure.js")
.script(graphitiPath+"src/shape/node/Node.js")
.script(graphitiPath+"src/VectorFigure.js")
.script(graphitiPath+"src/shape/basic/Rectangle.js")
.script(graphitiPath+"src/SetFigure.js")
.script(graphitiPath+"src/SVGFigure.js")
.script(graphitiPath+"src/shape/node/Hub.js").wait()
.script(graphitiPath+"src/shape/node/HorizontalBus.js")
.script(graphitiPath+"src/shape/node/VerticalBus.js")

.script(graphitiPath+"src/shape/basic/Oval.js")
.script(graphitiPath+"src/shape/basic/Circle.js")
.script(graphitiPath+"src/shape/basic/Label.js")
.script(graphitiPath+"src/shape/basic/Line.js")
.script(graphitiPath+"src/shape/basic/PolyLine.js")
.script(graphitiPath+"src/shape/basic/Diamond.js")
.script(graphitiPath+"src/shape/basic/Image.js")
.script(graphitiPath+"src/Connection.js")
.script(graphitiPath+"src/VectorFigure.js")
.script(graphitiPath+"src/ResizeHandle.js")
.script(graphitiPath+"src/LineResizeHandle.js")
.script(graphitiPath+"src/LineStartResizeHandle.js")
.script(graphitiPath+"src/LineEndResizeHandle.js")
.script(graphitiPath+"src/Port.js").wait()
.script(graphitiPath+"src/InputPort.js")
.script(graphitiPath+"src/OutputPort.js")
.script(graphitiPath+"src/HybridPort.js")
.script(graphitiPath+"src/layout/anchor/ConnectionAnchor.js")
.script(graphitiPath+"src/layout/anchor/ChopboxConnectionAnchor.js")
.script(graphitiPath+"src/layout/anchor/ShortesPathConnectionAnchor.js")

.script(graphitiPath+"src/shape/arrow/CalligrapherArrowLeft.js")
.script(graphitiPath+"src/shape/arrow/CalligrapherArrowDownLeft.js")
.script(graphitiPath+"src/shape/node/Start.js")
.script(graphitiPath+"src/shape/node/End.js")
.script(graphitiPath+"src/shape/node/Between.js")
.script(graphitiPath+"src/shape/note/PostIt.js")

.script(graphitiPath+"src/shape/widget/Widget.js")
.script(graphitiPath+"src/shape/widget/Slider.js")

.script(graphitiPath+"src/shape/diagram/Diagram.js")
.script(graphitiPath+"src/shape/diagram/Pie.js")
.script(graphitiPath+"src/shape/diagram/Sparkline.js")

.script(graphitiPath+"src/shape/analog/OpAmp.js")
.script(graphitiPath+"src/shape/analog/ResistorBridge.js")
.script(graphitiPath+"src/shape/analog/ResistorVertical.js")
.script(graphitiPath+"src/shape/analog/VoltageSupplyHorizontal.js")
.script(graphitiPath+"src/shape/analog/VoltageSupplyVertical.js")
.script(graphitiPath+"src/shape/icon/Icon.js")
.script(graphitiPath+"src/shape/icon/Thunder.js")
.script(graphitiPath+"src/shape/icon/Snow.js")
.script(graphitiPath+"src/shape/icon/Hail.js")
.script(graphitiPath+"src/shape/icon/Rain.js")
.script(graphitiPath+"src/shape/icon/Cloudy.js")
.script(graphitiPath+"src/shape/icon/Sun.js")
.script(graphitiPath+"src/shape/icon/Undo.js")
.script(graphitiPath+"src/shape/icon/Detour.js")
.script(graphitiPath+"src/shape/icon/Merge.js")
.script(graphitiPath+"src/shape/icon/Split.js")
.script(graphitiPath+"src/shape/icon/Fork.js")
.script(graphitiPath+"src/shape/icon/ForkAlt.js")
.script(graphitiPath+"src/shape/icon/Exchange.js")
.script(graphitiPath+"src/shape/icon/Shuffle.js")
.script(graphitiPath+"src/shape/icon/Refresh.js")
.script(graphitiPath+"src/shape/icon/Ccw.js")
.script(graphitiPath+"src/shape/icon/Acw.js")
.script(graphitiPath+"src/shape/icon/Contract.js")
.script(graphitiPath+"src/shape/icon/Expand.js")
.script(graphitiPath+"src/shape/icon/Stop.js")
.script(graphitiPath+"src/shape/icon/End.js")
.script(graphitiPath+"src/shape/icon/Start.js")
.script(graphitiPath+"src/shape/icon/Ff.js")
.script(graphitiPath+"src/shape/icon/Rw.js")
.script(graphitiPath+"src/shape/icon/ArrowRight.js")
.script(graphitiPath+"src/shape/icon/ArrowLeft.js")
.script(graphitiPath+"src/shape/icon/ArrowUp.js")
.script(graphitiPath+"src/shape/icon/ArrowDown.js")
.script(graphitiPath+"src/shape/icon/ArrowLeft2.js")
.script(graphitiPath+"src/shape/icon/ArrowRight2.js")
.script(graphitiPath+"src/shape/icon/Smile2.js")
.script(graphitiPath+"src/shape/icon/Smile.js")
.script(graphitiPath+"src/shape/icon/Alarm.js")
.script(graphitiPath+"src/shape/icon/Clock.js")
.script(graphitiPath+"src/shape/icon/StopWatch.js")
.script(graphitiPath+"src/shape/icon/History.js")
.script(graphitiPath+"src/shape/icon/Future.js")
.script(graphitiPath+"src/shape/icon/GlobeAlt2.js")
.script(graphitiPath+"src/shape/icon/GlobeAlt.js")
.script(graphitiPath+"src/shape/icon/Globe.js")
.script(graphitiPath+"src/shape/icon/Warning.js")
.script(graphitiPath+"src/shape/icon/Code.js")
.script(graphitiPath+"src/shape/icon/Pensil.js")
.script(graphitiPath+"src/shape/icon/Pen.js")
.script(graphitiPath+"src/shape/icon/Plus.js")
.script(graphitiPath+"src/shape/icon/Minus.js")
.script(graphitiPath+"src/shape/icon/TShirt.js")
.script(graphitiPath+"src/shape/icon/Sticker.js")
.script(graphitiPath+"src/shape/icon/Page2.js")
.script(graphitiPath+"src/shape/icon/Page.js")
.script(graphitiPath+"src/shape/icon/Landscape1.js")
.script(graphitiPath+"src/shape/icon/Landscape2.js")
.script(graphitiPath+"src/shape/icon/Plugin.js")
.script(graphitiPath+"src/shape/icon/Bookmark.js")
.script(graphitiPath+"src/shape/icon/Hammer.js")
.script(graphitiPath+"src/shape/icon/Users.js")
.script(graphitiPath+"src/shape/icon/User.js")
.script(graphitiPath+"src/shape/icon/Customer.js")
.script(graphitiPath+"src/shape/icon/Employee.js")
.script(graphitiPath+"src/shape/icon/Anonymous.js")
.script(graphitiPath+"src/shape/icon/Skull.js")
.script(graphitiPath+"src/shape/icon/Mail.js")
.script(graphitiPath+"src/shape/icon/Picture.js")
.script(graphitiPath+"src/shape/icon/Bubble.js")
.script(graphitiPath+"src/shape/icon/CodeTalk.js")
.script(graphitiPath+"src/shape/icon/Talkq.js")
.script(graphitiPath+"src/shape/icon/Talke.js")
.script(graphitiPath+"src/shape/icon/Home.js")
.script(graphitiPath+"src/shape/icon/Lock.js")
.script(graphitiPath+"src/shape/icon/Clip.js")
.script(graphitiPath+"src/shape/icon/Star.js")
.script(graphitiPath+"src/shape/icon/StarOff.js")
.script(graphitiPath+"src/shape/icon/Star2.js")
.script(graphitiPath+"src/shape/icon/Star2Off.js")
.script(graphitiPath+"src/shape/icon/Star3.js")
.script(graphitiPath+"src/shape/icon/Star3Off.js")
.script(graphitiPath+"src/shape/icon/Chat.js")
.script(graphitiPath+"src/shape/icon/Quote.js")
.script(graphitiPath+"src/shape/icon/Gear2.js")
.script(graphitiPath+"src/shape/icon/Gear.js")
.script(graphitiPath+"src/shape/icon/Wrench.js")
.script(graphitiPath+"src/shape/icon/Wrench2.js")
.script(graphitiPath+"src/shape/icon/Wrench3.js")
.script(graphitiPath+"src/shape/icon/ScrewDriver.js")
.script(graphitiPath+"src/shape/icon/HammerAndScrewDriver.js")
.script(graphitiPath+"src/shape/icon/Magic.js")
.script(graphitiPath+"src/shape/icon/Download.js")
.script(graphitiPath+"src/shape/icon/View.js")
.script(graphitiPath+"src/shape/icon/Noview.js")
.script(graphitiPath+"src/shape/icon/Cloud.js")
.script(graphitiPath+"src/shape/icon/Cloud2.js")
.script(graphitiPath+"src/shape/icon/CloudDown.js")
.script(graphitiPath+"src/shape/icon/CloudUp.js")
.script(graphitiPath+"src/shape/icon/Location.js")
.script(graphitiPath+"src/shape/icon/Volume0.js")
.script(graphitiPath+"src/shape/icon/Volume1.js")
.script(graphitiPath+"src/shape/icon/Volume2.js")
.script(graphitiPath+"src/shape/icon/Volume3.js")
.script(graphitiPath+"src/shape/icon/Key.js")
.script(graphitiPath+"src/shape/icon/Ruler.js")
.script(graphitiPath+"src/shape/icon/Power.js")
.script(graphitiPath+"src/shape/icon/Unlock.js")
.script(graphitiPath+"src/shape/icon/Flag.js")
.script(graphitiPath+"src/shape/icon/Tag.js")
.script(graphitiPath+"src/shape/icon/Search.js")
.script(graphitiPath+"src/shape/icon/ZoomOut.js")
.script(graphitiPath+"src/shape/icon/ZoomIn.js")
.script(graphitiPath+"src/shape/icon/Cross.js")
.script(graphitiPath+"src/shape/icon/Check.js")
.script(graphitiPath+"src/shape/icon/Settings.js")
.script(graphitiPath+"src/shape/icon/SettingsAlt.js")
.script(graphitiPath+"src/shape/icon/Feed.js")
.script(graphitiPath+"src/shape/icon/Bug.js")
.script(graphitiPath+"src/shape/icon/Link.js")
.script(graphitiPath+"src/shape/icon/Calendar.js")
.script(graphitiPath+"src/shape/icon/Picker.js")
.script(graphitiPath+"src/shape/icon/No.js")
.script(graphitiPath+"src/shape/icon/CommandLine.js")
.script(graphitiPath+"src/shape/icon/Photo.js")
.script(graphitiPath+"src/shape/icon/Printer.js")
.script(graphitiPath+"src/shape/icon/Export.js")
.script(graphitiPath+"src/shape/icon/Import.js")
.script(graphitiPath+"src/shape/icon/Run.js")
.script(graphitiPath+"src/shape/icon/Magnet.js")
.script(graphitiPath+"src/shape/icon/NoMagnet.js")
.script(graphitiPath+"src/shape/icon/ReflectH.js")
.script(graphitiPath+"src/shape/icon/ReflectV.js")
.script(graphitiPath+"src/shape/icon/Resize2.js")
.script(graphitiPath+"src/shape/icon/Rotate.js")
.script(graphitiPath+"src/shape/icon/Connect.js")
.script(graphitiPath+"src/shape/icon/Disconnect.js")
.script(graphitiPath+"src/shape/icon/Folder.js")
.script(graphitiPath+"src/shape/icon/Man.js")
.script(graphitiPath+"src/shape/icon/Woman.js")
.script(graphitiPath+"src/shape/icon/People.js")
.script(graphitiPath+"src/shape/icon/Parent.js")
.script(graphitiPath+"src/shape/icon/Notebook.js")
.script(graphitiPath+"src/shape/icon/Diagram.js")
.script(graphitiPath+"src/shape/icon/BarChart.js")
.script(graphitiPath+"src/shape/icon/PieChart.js")
.script(graphitiPath+"src/shape/icon/LineChart.js")
.script(graphitiPath+"src/shape/icon/Apps.js")
.script(graphitiPath+"src/shape/icon/Locked.js")
.script(graphitiPath+"src/shape/icon/Ppt.js")
.script(graphitiPath+"src/shape/icon/Lab.js")
.script(graphitiPath+"src/shape/icon/Umbrella.js")
.script(graphitiPath+"src/shape/icon/Dry.js")
.script(graphitiPath+"src/shape/icon/Ipad.js")
.script(graphitiPath+"src/shape/icon/Iphone.js")
.script(graphitiPath+"src/shape/icon/Jigsaw.js")
.script(graphitiPath+"src/shape/icon/Lamp.js")
.script(graphitiPath+"src/shape/icon/Lamp_alt.js")
.script(graphitiPath+"src/shape/icon/Video.js")
.script(graphitiPath+"src/shape/icon/Palm.js")
.script(graphitiPath+"src/shape/icon/Fave.js")
.script(graphitiPath+"src/shape/icon/Help.js")
.script(graphitiPath+"src/shape/icon/Crop.js")
.script(graphitiPath+"src/shape/icon/BioHazard.js")
.script(graphitiPath+"src/shape/icon/WheelChair.js")
.script(graphitiPath+"src/shape/icon/Mic.js")
.script(graphitiPath+"src/shape/icon/MicMute.js")
.script(graphitiPath+"src/shape/icon/IMac.js")
.script(graphitiPath+"src/shape/icon/Pc.js")
.script(graphitiPath+"src/shape/icon/Cube.js")
.script(graphitiPath+"src/shape/icon/FullCube.js")
.script(graphitiPath+"src/shape/icon/Font.js")
.script(graphitiPath+"src/shape/icon/Trash.js")
.script(graphitiPath+"src/shape/icon/NewWindow.js")
.script(graphitiPath+"src/shape/icon/DockRight.js")
.script(graphitiPath+"src/shape/icon/DockLeft.js")
.script(graphitiPath+"src/shape/icon/DockBottom.js")
.script(graphitiPath+"src/shape/icon/DockTop.js")
.script(graphitiPath+"src/shape/icon/Pallete.js")
.script(graphitiPath+"src/shape/icon/Cart.js")
.script(graphitiPath+"src/shape/icon/Glasses.js")
.script(graphitiPath+"src/shape/icon/Package.js")
.script(graphitiPath+"src/shape/icon/Book.js")
.script(graphitiPath+"src/shape/icon/Books.js")
.script(graphitiPath+"src/shape/icon/Icons.js")
.script(graphitiPath+"src/shape/icon/List.js")
.script(graphitiPath+"src/shape/icon/Db.js")
.script(graphitiPath+"src/shape/icon/Paper.js")
.script(graphitiPath+"src/shape/icon/TakeOff.js")
.script(graphitiPath+"src/shape/icon/Landing.js")
.script(graphitiPath+"src/shape/icon/Plane.js")
.script(graphitiPath+"src/shape/icon/Phone.js")
.script(graphitiPath+"src/shape/icon/HangUp.js")
.script(graphitiPath+"src/shape/icon/SlideShare.js")
.script(graphitiPath+"src/shape/icon/Twitter.js")
.script(graphitiPath+"src/shape/icon/TwitterBird.js")
.script(graphitiPath+"src/shape/icon/Skype.js")
.script(graphitiPath+"src/shape/icon/Windows.js")
.script(graphitiPath+"src/shape/icon/Apple.js")
.script(graphitiPath+"src/shape/icon/Linux.js")
.script(graphitiPath+"src/shape/icon/NodeJs.js")
.script(graphitiPath+"src/shape/icon/JQuery.js")
.script(graphitiPath+"src/shape/icon/Sencha.js")
.script(graphitiPath+"src/shape/icon/Vim.js")
.script(graphitiPath+"src/shape/icon/InkScape.js")
.script(graphitiPath+"src/shape/icon/Aumade.js")
.script(graphitiPath+"src/shape/icon/Firefox.js")
.script(graphitiPath+"src/shape/icon/Ie.js")
.script(graphitiPath+"src/shape/icon/Ie9.js")
.script(graphitiPath+"src/shape/icon/Opera.js")
.script(graphitiPath+"src/shape/icon/Chrome.js")
.script(graphitiPath+"src/shape/icon/Safari.js")
.script(graphitiPath+"src/shape/icon/LinkedIn.js")
.script(graphitiPath+"src/shape/icon/Flickr.js")
.script(graphitiPath+"src/shape/icon/GitHub.js")
.script(graphitiPath+"src/shape/icon/GitHubAlt.js")
.script(graphitiPath+"src/shape/icon/Raphael.js")
.script(graphitiPath+"src/shape/icon/GRaphael.js")
.script(graphitiPath+"src/shape/icon/Svg.js")
.script(graphitiPath+"src/shape/icon/Usb.js")
.script(graphitiPath+"src/shape/icon/Ethernet.js")
   
.script(graphitiPath+"src/ui/LabelEditor.js")
.script(graphitiPath+"src/ui/LabelInplaceEditor.js")

.script(graphitiPath+"src/decoration/connection/Decorator.js")
.script(graphitiPath+"src/decoration/connection/ArrowDecorator.js")
.script(graphitiPath+"src/decoration/connection/DiamondDecorator.js")
.script(graphitiPath+"src/decoration/connection/CircleDecorator.js")
.script(graphitiPath+"src/decoration/connection/BarDecorator.js")

.script(graphitiPath+"src/io/Reader.js")
.script(graphitiPath+"src/io/Writer.js")
.script(graphitiPath+"src/io/svg/Writer.js")
.script(graphitiPath+"src/io/png/Writer.js")
.script(graphitiPath+"src/io/json/Writer.js")
.script(graphitiPath+"src/io/json/Reader.js").wait(
function(){
	var link = $("<link>");
	link.attr({
	        type: 'text/css',
	        rel: 'stylesheet',
	        href: graphitiPath+"css/contextmenu.css"
	});
	$("head").append( link ); 

	// avoid iPad bounce effect during DragDrop
	//
    document.ontouchmove = function(e){e.preventDefault();};

    // hide context menu
    document.oncontextmenu = function() {return false;};
    
    
    // hacking RaphaelJS to support groups of elements
	//
	(function() {
	    Raphael.fn.group = function(f, g) {
	        var enabled = document.getElementsByTagName("svg").length > 0;
	        if (!enabled) {
	            // return a stub for VML compatibility
	            return {
	                add : function() {
	                    // intentionally left blank
	                }
	            };
	        }
	      var i;
	      this.svg = "http://www.w3.org/2000/svg";
	      this.defs = document.getElementsByTagName("defs")[f];
	      this.svgcanv = document.getElementsByTagName("svg")[f];
	      this.group = document.createElementNS(this.svg, "g");
	      for(i = 0;i < g.length;i++) {
	        this.group.appendChild(g[i].node);
	      }
	      this.svgcanv.appendChild(this.group);
	      this.group.translate = function(c, a) {
	        this.setAttribute("transform", "translate(" + c + "," + a + ") scale(" + this.getAttr("scale").x + "," + this.getAttr("scale").y + ")");
	      };
	      this.group.rotate = function(c, a, e) {
	        this.setAttribute("transform", "translate(" + this.getAttr("translate").x + "," + this.getAttr("translate").y + ") scale(" + this.getAttr("scale").x + "," + this.getAttr("scale").y + ") rotate(" + c + "," + a + "," + e + ")");
	      };
	      this.group.scale = function(c, a) {
	        this.setAttribute("transform", "scale(" + c + "," + a + ") translate(" + this.getAttr("translate").x + "," + this.getAttr("translate").y + ")");
	      };
	      this.group.push = function(c) {
	        this.appendChild(c.node);
	      };
	      this.group.getAttr = function(c) {
	        this.previous = this.getAttribute("transform") ? this.getAttribute("transform") : "";
	        var a = [], e, h, j;
	        a = this.previous.split(" ");
	        for(i = 0;i < a.length;i++) {
	          if(a[i].substring(0, 1) == "t") {
	            var d = a[i], b = [];
	            b = d.split("(");
	            d = b[1].substring(0, b[1].length - 1);
	            b = [];
	            b = d.split(",");
	            e = b.length === 0 ? {x:0, y:0} : {x:b[0], y:b[1]};
	          }else {
	            if(a[i].substring(0, 1) === "r") {
	              d = a[i];
	              b = d.split("(");
	              d = b[1].substring(0, b[1].length - 1);
	              b = d.split(",");
	              h = b.length === 0 ? {x:0, y:0, z:0} : {x:b[0], y:b[1], z:b[2]};
	            }else {
	              if(a[i].substring(0, 1) === "s") {
	                d = a[i];
	                b = d.split("(");
	                d = b[1].substring(0, b[1].length - 1);
	                b = d.split(",");
	                j = b.length === 0 ? {x:1, y:1} : {x:b[0], y:b[1]};
	              }
	            }
	          }
	        }
	        if(typeof e === "undefined") {
	          e = {x:0, y:0};
	        }
	        if(typeof h === "undefined") {
	          h = {x:0, y:0, z:0};
	        }
	        if(typeof j === "undefined") {
	          j = {x:1, y:1};
	        }
	        
	        if(c == "translate") {
	          var k = e;
	        }else {
	          if(c == "rotate") {
	            k = h;
	          }else {
	            if(c == "scale") {
	              k = j;
	            }
	          }
	        }
	        return k;
	      };
	      this.group.copy = function(el){
	         this.copy = el.node.cloneNode(true);
	         this.appendChild(this.copy);
	      };
	      return this.group;
	    };
	})();
	try{
	graphitiLoaded();
	}catch(exc){
	    console.log(exc);
	}
});

var _errorStack_=[];
function pushErrorStack(/*:Exception*/ e, /*:String*/ functionName)
{
  _errorStack_.push(functionName+"\n");
  /*re*/throw e;
}


Math.sign = function()
{
 if (this < 0) {return -1;}
 return 1;
};
