import jsonfield
from datetime import datetime
from django.db import models

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class BlockMap(models.Model):
    name = models.CharField(max_length=200)
    json = jsonfield.JSONField()
    created_by = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-created_at"]

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        self.modified = datetime.now()
        super(BlockMap, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('blockmap_edit', kwargs={'pk': self.pk})


# TODO: Defne custom methods here
