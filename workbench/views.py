import json
from django.http import HttpResponse
from django.views.generic import ListView, CreateView, DeleteView, TemplateView
from django.core.urlresolvers import reverse_lazy

from workbench.models import BlockMap
from workbench.forms import BlockMapForm

class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return self.render_to_json_response(form.errors, status=400)
        else:
            return super(AjaxableResponseMixin, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            data = {
                'pk': form.instance.pk,
            }
            return self.render_to_json_response(data)
        else:
            return super(AjaxableResponseMixin, self).form_valid(form)


class BlockMapCreate(AjaxableResponseMixin, CreateView):
    form_class = BlockMapForm
    template_name = "blockmap/blockmap_create.html"
    model = BlockMap

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(BlockMapCreate, self).form_valid(form)


class BlockMapEdit(TemplateView):
    model = BlockMap
    template_name = "blockmap/blockmap_edit.html"

    def get_context_data(self, **kwargs):
        context = super(BlockMapEdit, self).get_context_data(**kwargs)
        context['blockmap'] = BlockMap.objects.get(pk=self.kwargs.get('pk', None))
        return context


class BlockMapDelete(DeleteView):
    model = BlockMap
    success_url = reverse_lazy('home')


class BlockMapListView(ListView):
    template_name = "blockmap/blockmap_list.html"
    model = BlockMap

    def get_queryset(self):
        if self.request.user.is_authenticated():
            return BlockMap.objects.filter(created_by=self.request.user.pk)
